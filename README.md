**Packaging for OpenSSL**

## Example CMake Usage:
```cmake
target_link_libraries(target ssl crypto)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:ssl,INTERFACE_INCLUDE_DIRECTORIES>")

#include "openssl/ssl.h"
find_package(Threads)
target_link_libraries(target ${CMAKE_THREAD_LIBS_INIT})
if (WIN32)
    target_link_libraries(target crypt32)
endif ()

#include "openssl/err.h"
if (UNIX)
    target_link_libraries(target dl)
endif

#include "openssl/bio.h"
if (WIN32)
    target_link_libraries(target crypt32 ws2_32)
endif ()
```