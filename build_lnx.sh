#!/usr/bin/env bash

# Usage:
# build_lnx <operating system> <architecture> <config>

# Exit as failure if any command fails
set -e

BASE_DIR="`cd \`dirname ${BASH_SOURCE}\` && pwd`"
OS="$1"
ARCH="$2"
CONFIG="$3"

rm -fr "${BASE_DIR}/${OS} ${ARCH}/${CONFIG}"
mkdir -p "${BASE_DIR}/${OS} ${ARCH}/${CONFIG}"

pushd "${BASE_DIR}/${OS} ${ARCH}/${CONFIG}"

    # Turning off afalg as it is not in CentOS
    CONFIG_ARGS="no-afalgeng no-shared no-zlib no-zlib-dynamic"

    if [[ "${CONFIG}" == "Debug" ]]; then
        CONFIG_ARGS="${CONFIG_ARGS} -d"
    fi

    ../../source/config ${CONFIG_ARGS}
    make -j `nproc`

popd
