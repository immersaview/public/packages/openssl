param (
    [Parameter(Mandatory = ${true})][string]${Arch},
    [Parameter(Mandatory = ${true})][string]${Config}
)

if (${Arch} -eq 'x64') {
    ${VsDevCmdArch} = "-arch=amd64 -host_arch=amd64"
}

# MS vswhere load environment example (https://github.com/Microsoft/vswhere/wiki/Start-Developer-Command-Prompt)
${InstallationPath} = &"${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer/vswhere.exe" -Products 'Microsoft.VisualStudio.Product.BuildTools' -Prerelease -Latest -Property installationPath
if (${InstallationPath} -And (Test-Path "${InstallationPath}\Common7\Tools\vsdevcmd.bat")) {
    & "${env:COMSPEC}" /s /c "`"${InstallationPath}\Common7\Tools\vsdevcmd.bat`" ${VsDevCmdArch} -no_logo && set" | Foreach-Object {
        ${Name}, ${Value} = $_ -Split '=', 2
        Set-Content env:\"${Name}" ${Value}
    }
}

# Ignore errors above from MS example and possibly hack above
${ErrorActionPreference} = "Stop"

if (${Arch} -eq 'x64') {
    if (${Config} -eq 'Debug') {
        ${ConfigurePlatform} = "debug-VC-WIN64A"
    }
    else {
        ${ConfigurePlatform} = "VC-WIN64A"
    }
}
else {
    if (${Config} -eq 'Debug') {
        ${ConfigurePlatform} = "debug-VC-WIN32"
    }
    else {
        ${ConfigurePlatform} = "VC-WIN32"
    }
}

Remove-Item -path "${PSScriptRoot}\Windows ${Arch}\${Config}" -Force -Recurse -ErrorAction SilentlyContinue
New-Item -path "${PSScriptRoot}\Windows ${Arch}\${Config}" -ItemType Directory

Push-Location "${PSScriptRoot}\Windows ${Arch}\${Config}"

perl ..\..\source\Configure no-shared no-zlib no-zlib-dynamic no-asm ${ConfigurePlatform}

# HACK: this is a hack to replace /Zi flags with /Z7, this should be revisited for a better solution.
(Get-Content .\makefile).replace('/Zi', '/Z7 /MP') | Set-Content .\makefile
nmake

Pop-Location
