stages:
  - build
  - package
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: recursive

#-----------------------------------------------------------
#                      TEMPLATES
#-----------------------------------------------------------

.build_win_template: &win_build_definition
  stage: build
  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-openssl-build-deps:1.0.0-windows2019
  tags:
    - imv-windows-docker
  artifacts:
    expire_in: 1 week
    paths:
      - "${OS} ${ARCH}/${CONFIG}/*.lib"
      - "${OS} ${ARCH}/${CONFIG}/include/openssl/opensslconf.h"
  script:
    - powershell ./build_win.ps1 -Arch ${ARCH} -Config ${CONFIG}

.build_lnx_template: &lnx_build_definition
  stage: build
  artifacts:
    expire_in: 4 hours
    paths:
      - "$OS $ARCH/$CONFIG/*.a"
      - "$OS $ARCH/$CONFIG/include/openssl/opensslconf.h"
  script:
    - bash ./build_lnx.sh $OS $ARCH $CONFIG

#-----------------------------------------------------------
#                      WINDOWS BUILDS
#-----------------------------------------------------------

build_win_vs2019_x64_rel:
  <<: *win_build_definition

  variables:
    OS: "Windows"
    ARCH: "x64"
    CONFIG: "Release"

build_win_vs2019_x86_rel:
  <<: *win_build_definition

  variables:
    OS: "Windows"
    ARCH: "x86"
    CONFIG: "Release"

build_win_vs2019_x64_dbg:
  <<: *win_build_definition

  variables:
    OS: "Windows"
    ARCH: "x64"
    CONFIG: "Debug"

build_win_vs2019_x86_dbg:
  <<: *win_build_definition

  variables:
    OS: "Windows"
    ARCH: "x86"
    CONFIG: "Debug"

#-----------------------------------------------------------
#                      CENTOS 7 BUILDS
#-----------------------------------------------------------

build_centos7_x64_rel:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7
  variables:
    OS: "CentOS7"
    ARCH: "x64"
    CONFIG: "Release"

build_centos7_x64_dbg:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.1.3-centos7
  variables:
    OS: "CentOS7"
    ARCH: "x64"
    CONFIG: "Debug"

#-----------------------------------------------------------
#                      UBUNTU 20.04 BUILDS
#-----------------------------------------------------------

build_ubuntu20_04_x64_rel:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.0.0-ubuntu20_04
  variables:
    OS: "Ubuntu20_04"
    ARCH: "x64"
    CONFIG: "Release"

build_ubuntu20_04_x64_dbg:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.0.0-ubuntu20_04
  variables:
    OS: "Ubuntu20_04"
    ARCH: "x64"
    CONFIG: "Debug"

#-----------------------------------------------------------
#                      UBUNTU 18.04 BUILDS
#-----------------------------------------------------------

build_ubuntu18_04_x64_rel:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  variables:
    OS: "Ubuntu18_04"
    ARCH: "x64"
    CONFIG: "Release"

build_ubuntu18_04_x64_dbg:
  <<: *lnx_build_definition

  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  variables:
    OS: "Ubuntu18_04"
    ARCH: "x64"
    CONFIG: "Debug"

#-----------------------------------------------------------
#                      UBUNTU 16.04 BUILDS
#-----------------------------------------------------------

# build_deb_arm64_rel:
#   <<: *lnx_build_definition

#   tags:
#     - arm64
#     - ubuntu-16-04
#   variables:
#     OS: "Ubuntu16_04"
#     ARCH: "arm64"
#     CONFIG: "Release"

# build_deb_arm64_dbg:
#   <<: *lnx_build_definition

#   tags:
#     - arm64
#     - ubuntu-16-04
#   variables:
#     OS: "Ubuntu16_04"
#     ARCH: "arm64"
#     CONFIG: "Debug"

#-----------------------------------------------------------
#                      PACKAGE
#-----------------------------------------------------------

package:
  stage: package
  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  script:
    - pushd NuGet
    - bash ./check_package_exists.sh
    - nuget pack -Symbols
    - popd
  artifacts:
    expire_in: 1 week
    paths:
      - "NuGet/*.nupkg"

#-----------------------------------------------------------
#                      DEPLOY
#-----------------------------------------------------------

deploy:
  stage: deploy
  image: registry.gitlab.com/immersaview/public/ci-docker-images/ci-cpp-build-deps:1.5.0-ubuntu18_04
  script:
    - pushd NuGet
    - nuget source Add -Name gitlab -Source "$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/packages/nuget/index.json" -UserName gitlab-ci-token -Password $CI_JOB_TOKEN
    - nuget push *.symbols.nupkg -Source gitlab
    - popd
  only:
    - master
  variables:
    GIT_STRATEGY: none
